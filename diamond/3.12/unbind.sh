#!/bin/bash

USB_PATHS=`ls -d /sys/bus/usb/drivers/ftdi_sio/*.1 2> /dev/null`
FTDI_UNBIND="/sys/bus/usb/drivers/ftdi_sio/unbind"

for USB_PATH in $USB_PATHS
do
    echo ${USB_PATH}
    USB_PART=`basename $USB_PATH`
    echo ${USB_PART}
    while true
    do
        read -p "Unbind ${USB_PART} from ftdi_sio? [yn]: " RESPONSE
        if [ ${RESPONSE} == "y" ] || [ ${RESPONSE} == "n" ]
        then
            break
        fi
    done

    if [ ${RESPONSE} == "y" ]
    then
        echo "Unbinding ${USB_PART}"
        echo ${USB_PART} > ${FTDI_UNBIND}
    fi
done
