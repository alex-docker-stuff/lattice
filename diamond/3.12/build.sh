#!/bin/bash -e

NO_INST=$1

ORG=lattice
THING=diamond
VERSION=3.12

FILE=diamond_3_12-base-240-2-x86_64-linux.rpm

while [ ! -f build/${FILE} ]
do
    echo "Get ${FILE} from bas at"
    echo "~orange/setup_files/${ORG}/${THING}/${VERSION}/ and put in build/inst/"
    read
done

FILE=diamond_3_12-sp1-454-2-x86_64-linux.rpm

while [ ! -f build/${FILE} ]
do
    echo "Get ${FILE} from bas at"
    echo "~orange/setup_files/${ORG}/${THING}/${VERSION}/ and put in build/inst/"
    read
done

IMAGE_NAME=${ORG}/${THING}:${VERSION}

DOCKER_REGISTRY_AUTH=docker-registry-auth.k8s.flux.utah.edu
DOCKER_REGISTRY=docker-registry.k8s.flux.utah.edu

docker build -f build/Dockerfile -t ${DOCKER_REGISTRY_AUTH}/$IMAGE_NAME build
docker push ${DOCKER_REGISTRY_AUTH}/$IMAGE_NAME
docker tag ${DOCKER_REGISTRY_AUTH}/$IMAGE_NAME ${DOCKER_REGISTRY}/$IMAGE_NAME
