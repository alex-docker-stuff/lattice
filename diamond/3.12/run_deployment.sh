#!/bin/bash

if [ ! -d "home" ]; then
    mkdir home
fi


USB_PATHS=`ls -d /sys/bus/usb/drivers/ftdi_sio/*.1 2> /dev/null`
FTDI_UNBIND="/sys/bus/usb/drivers/ftdi_sio/unbind"

for USB_PATH in $USB_PATHS
do
    echo ${USB_PATH}
    USB_PART=`basename $USB_PATH`
    echo ${USB_PART}
    while true
    do
        read -p "Unbind ${USB_PART} from ftdi_sio (uses sudo)? [yn]: " RESPONSE
        if [ ${RESPONSE} == "y" ] || [ ${RESPONSE} == "n" ]
        then
            break
        fi
    done

    if [ ${RESPONSE} == "y" ]
    then
        echo "Unbinding ${USB_PART}"
        sudo bash -c "echo ${USB_PART} > ${FTDI_UNBIND}"
    fi
done


VGLUSERS_GID=`getent group vglusers | cut -d ":" -f 3`
USB_GROUP_GID=`getent group plugdev | cut -d ":" -f 3`

GROUPS_ADD=$VGLUSERS_GID

docker run --rm -it \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /var/lib/sss/pipes/:/var/lib/sss/pipes/:rw \
    --mount type=bind,src=`readlink \
    -f ./home`,dst=$HOME \
    -e DISPLAY=unix$DISPLAY \
    -e HOME=$HOME -u `id -u`:`id -g` \
    --group-add $VGLUSERS_GID \
    --group-add $USB_GROUP_GID \
    --mac-address="02:42:de:ad:be:ef" \
    --mount type=bind,src=/dev/bus/usb,dst=/dev/bus/usb \
    --privileged \
    docker-registry.k8s.flux.utah.edu/lattice/diamond:3.12 \
    /usr/local/diamond/3.12/bin/lin64/deployment
